/**
 * @file motors.c
 *
 * @date 19.11.2015
 *
 * @author Jacek Jankowski
 */

#include "motors.h"

extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;

int32_t v, omega, v_d, omega_d, pwm_left, pwm_right;
osThreadId velocity_task_handle;

void pwm(int8_t left, int8_t right);

int32_t return_omega(void){
	return omega;
}

int32_t return_v(void){
	return v;
}

void set_v_omega(int32_t new_v, int32_t new_omega){
	v_d = new_v;
	omega_d = new_omega;
}


void pwm(int8_t left, int8_t right){


	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_14, (left < 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, (left < 0) ? GPIO_PIN_RESET : GPIO_PIN_SET);

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (right < 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (right < 0) ? GPIO_PIN_RESET : GPIO_PIN_SET);

	if(left < 0){
		left *= -1;
	}

	if(right < 0){
		right *= -1;
	}

	left = (left >= 100) ? 99 : left;
	right = (right >= 100) ? 99 : right;

	TIM4->CCR1 = left;
	TIM4->CCR2 = right;
}

void velocity_task(void const * argument){
	const int32_t k = 3;
	const int32_t pwm_limit = 50;

	HAL_TIM_Base_Start(&htim2);
	HAL_TIM_Base_Start(&htim3);
	HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);

	for(;;){
		int32_t phi_dot_left = htim2.Instance->CNT - 30000;
		TIM2->CNT = 30000;
		int32_t phi_dot_right = htim3.Instance->CNT - 30000;
		TIM3->CNT = 30000;
		v = phi_dot_left + phi_dot_right;
		omega = phi_dot_right - phi_dot_left;

		pwm_right = k*((v_d-v) + (omega_d-omega));
		pwm_left = k*((v_d-v) - (omega_d-omega));

		pwm_left = (pwm_left > pwm_limit) ? pwm_limit : pwm_left;
		pwm_right = (pwm_right > pwm_limit) ? pwm_limit : pwm_right;
		pwm_left = (pwm_left < -pwm_limit) ? -pwm_limit : pwm_left;
		pwm_right = (pwm_right < -pwm_limit) ? -pwm_limit : pwm_right;


		pwm(pwm_left, pwm_right);

		osDelay(10);
	}
}
