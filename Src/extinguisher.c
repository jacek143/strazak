/**
 * @file extinguisher.c
 *
 * @date 19.11.2015
 *
 * @author Jacek Jankowski
 */

#include "extinguisher.h"
#include "mlx90614_baa.h"

int exti_flag = 0;

extern ADC_HandleTypeDef hadc1;
extern I2C_HandleTypeDef hi2c2;

uint16_t tab_ir[4];
uint16_t tab_temp_obj[4];
uint16_t tab_temp_amb[4];

void read_sensors(void){
	uint16_t address;
	for(uint8_t i = 0; i < 4; i++){

		switch(i){
		case 0:
			address = 0x5b;
			break;
		case 1:
			address = 0x5d;
			break;
		case 2:
			address = 0x5c;
			break;
		default: // stop
			address = 0x5a;
			break;
		} // switch

		mlx90614_baa_change_used_device(address);
		mlx90614_baa_read(&hi2c2, tab_temp_amb+i, tab_temp_obj+i);
		HAL_ADC_Start(&hadc1);
		HAL_ADC_PollForConversion(&hadc1, 1000);
		tab_ir[i] = HAL_ADC_GetValue(&hadc1);

	} // for
}

void extinguisher(bool turn){
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, turn ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

void led(bool turn){
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, turn ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

decision_t decide(void){
	static decision_t retval = rotate_left;
	uint16_t ir_high_threshold = 4000;
	uint16_t ir_low_threshold = 200;
	int16_t temp_threshold = 200;
	int16_t tab_temp_diff[4];
	int16_t temp_threshold_go_slow = 150;
	int16_t ir_threshold_go_slow = 1000;

	for(uint8_t i = 0; i < 4; i++){
		tab_temp_diff[i] = tab_temp_obj[i] - tab_temp_amb[i];
	} // for

	for(uint8_t i = 0; i < 4; i++){
		if(tab_ir[i] <= ir_high_threshold){
			led(1);

			for(uint8_t j = 0; j < 4; j++){
				if(tab_temp_diff[j] >= temp_threshold && tab_ir[j] <= ir_low_threshold){
					return retval = extinguish;
				} // if
			} // for

			for(uint8_t j = 0; j < 4; j++){
				if(tab_temp_diff[j] >= temp_threshold_go_slow || tab_ir[j] <= ir_threshold_go_slow){
					if(tab_ir[0] <= tab_ir[1] && tab_ir[0] <= tab_ir[2] && tab_ir[0] <= tab_ir[3]){
						return retval = rotate_right;
					} // if
					else if(tab_ir[3] <= tab_ir[0] && tab_ir[3] <= tab_ir[1] && tab_ir[3] <= tab_ir[2]){
						return retval = rotate_left;
					} // else if
					else{
						if(tab_temp_diff[1] < tab_temp_diff[2]){
							return retval = turn_left_slow;
						} // if
						else if(tab_temp_diff[2] < tab_temp_diff[1]){
							return retval = turn_right_slow;
						} // else if
						else{
							return retval = go_slow;
						} // else
					} // else
				} // if
			} // for

			if(tab_ir[0] <= tab_ir[1] && tab_ir[0] <= tab_ir[2] && tab_ir[0] <= tab_ir[3]){
				return retval = rotate_right;
			} // if
			else if(tab_ir[3] <= tab_ir[0] && tab_ir[3] <= tab_ir[1] && tab_ir[3] <= tab_ir[2]){
				return retval = rotate_left;
			} // else if
			else{
				if(tab_ir[2] < tab_ir[1]){
					return retval = turn_left;
				} // if
				else if(tab_ir[1] < tab_ir[2]){
					return retval = turn_right;
				} // else if
				else{
					return retval = go;
				} // else
			} // else
		} // if
	} // for

	if(retval != rotate_right && retval != rotate_left){
		retval = rotate_left;
	}
	led(0);
	return retval;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	exti_flag = !exti_flag;
}
