/**
 * @file info.c
 *
 * @date 26.11.2015
 *
 * @author Jacek Jankowski
 */

#include "info.h"

extern UART_HandleTypeDef huart1;

osThreadId info_task_handle;

void info_task(void const * argument){
	uint8_t frame[26];
	unsigned f = 0;

	for(;;){
		f = 0;

		frame[f++] = '!';

		for(unsigned i = 0; i < 4; i++){
			frame[f++] = tab_ir[3-i];
			frame[f++] = tab_ir[3-i] >> 8;
		}

		for(unsigned i = 0; i < 4; i++){
			frame[f++] = tab_temp_obj[3-i];
			frame[f++] = tab_temp_obj[3-i] >> 8;
		}

		for(unsigned i = 0; i < 4; i++){
			frame[f++] = tab_temp_amb[3-i];
			frame[f++] = tab_temp_amb[3-i] >> 8;
		}

		uint8_t crc = 0;
		for(unsigned i = 0; i < f; i++){
			crc = crc8(crc, frame[i]);
		}

		frame[f++] = crc;

		HAL_UART_Transmit_IT(&huart1, frame, f);
		osDelay(500);
	}
}
