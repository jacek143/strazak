/**
 * @file mlx90614_baa.c
 *
 * @date 05.11.2015
 *
 * @author Jacek Jankowski
 */

#include "mlx90614_baa.h"

uint16_t mlx90614_baa_address = 0x80;

const uint16_t ta_register = 0x06;
const uint16_t to_register = 0x07;
const uint16_t address_register = 0x2e;

const uint32_t i2c_timeout = 1000;

int I2CReadWord(I2C_HandleTypeDef * phi2c, uint8_t reg, uint16_t * dest);
int I2CWriteWord(I2C_HandleTypeDef * phi2c, uint8_t reg, uint16_t data);
int writeEEPROM(I2C_HandleTypeDef * phi2c, uint8_t reg, uint16_t data);

void mlx90614_baa_change_used_device(uint16_t new_device_address){
	mlx90614_baa_address = new_device_address;
}

float mlx90614_baa_return_temp(uint16_t temp_data){
	return temp_data / 50.0 - 273.15;
}

int mlx90614_baa_read(I2C_HandleTypeDef * phi2c, uint16_t * pta, uint16_t * pto) {

	if (!pta && !pto && !phi2c) {
		return 1;
	}

	if(I2CReadWord(phi2c, ta_register, pta)){
		return 1;
	}
	if(I2CReadWord(phi2c, to_register, pto)){
		return 1;
	}

	return 0;
}

int I2CReadWord(I2C_HandleTypeDef * phi2c, uint8_t reg, uint16_t * dest)
{
	uint8_t data[3];

	if (dest && phi2c) {
		if (HAL_I2C_Mem_Read(phi2c, mlx90614_baa_address << 1, reg, 1, data, 3, i2c_timeout) != HAL_OK) {
			return 1;
		}
	}


	uint8_t lsb = data[0];
	uint8_t msb = data[1];
	uint8_t pec = data[2];

	uint8_t crc = crc8(0, (mlx90614_baa_address << 1));
	crc = crc8(crc, reg);
	crc = crc8(crc, (mlx90614_baa_address << 1) + 1);
	crc = crc8(crc, lsb);
	crc = crc8(crc, msb);

	if (crc != pec)
	{
		return 1;
	}

	*dest = (msb << 8) | lsb;
	return 0;
}

int I2CWriteWord(I2C_HandleTypeDef * phi2c, uint8_t reg, uint16_t data)
{
	uint8_t crc;
	uint8_t lsb = data & 0x00FF;
	uint8_t msb = (data >> 8);
	uint8_t frame[4];

	if(!phi2c){
		return 1;
	}

	crc = crc8(0, (mlx90614_baa_address << 1));
	crc = crc8(crc, reg);
	crc = crc8(crc, lsb);
	crc = crc8(crc, msb);

	frame[0] = lsb;
	frame[1] = msb;
	frame[2] = crc;

	if(HAL_I2C_Mem_Write(phi2c, mlx90614_baa_address << 1, reg, 1, frame, 3, i2c_timeout) != HAL_OK){
		return 1;
	}

	return 0;
}

int writeEEPROM(I2C_HandleTypeDef * phi2c, uint8_t reg, uint16_t data)
{

	// Clear out EEPROM first:
	if (I2CWriteWord(phi2c, reg, 0))
		return 1; // If the write failed, return 1
	HAL_Delay(100); // Delay tErase

	uint8_t i2cRet = I2CWriteWord(phi2c, reg, data);
	HAL_Delay(100); // Delay tWrite

	return i2cRet;
}

int mlx90614_baa_setAddress(I2C_HandleTypeDef * phi2c, uint8_t newAdd)
{
	uint16_t tempAdd;
	// Make sure the address is within the proper range:
	if ((newAdd >= 0x80) || (newAdd == 0x00))
		return 1; // Return fail if out of range
	// Read from the I2C address address first:
	if (!I2CReadWord(phi2c, address_register, &tempAdd))
	{
		tempAdd &= 0xFF00; // Mask out the address (MSB is junk?)
		tempAdd |= newAdd; // Add the new address

		// Write the new addres back to EEPROM:
		if(writeEEPROM(phi2c, address_register, tempAdd)){
			return 1;
		}

		return 0;
	}
	return 1;
}
