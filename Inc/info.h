/**
 * @file info.h
 *
 * @date 26.11.2015
 *
 * @author Jacek Jankowski
 */

#ifndef INFO_HH
#define INFO_HH

#include "stm32f1xx_hal.h"
#include <stdint.h>
#include "cmsis_os.h"
#include "extinguisher.h"
#include "crc.h"

extern osThreadId info_task_handle;

void info_task(void const * argument);

#endif /* APPLICATION_USER_MLX90614_BAA_H_ */
