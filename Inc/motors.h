/**
 * @file motors.h
 *
 * @date 19.11.2015
 *
 * @author Jacek Jankowski
 */

#ifndef MOTORS_HH
#define MOTORS_HH

#include "stm32f1xx_hal.h"
#include <stdint.h>
#include "cmsis_os.h"

extern osThreadId velocity_task_handle;

int32_t return_omega(void);
int32_t return_v(void);
void set_v_omega(int32_t new_v, int32_t new_omega);
void velocity_task(void const * argument);

#endif /* APPLICATION_USER_MLX90614_BAA_H_ */
