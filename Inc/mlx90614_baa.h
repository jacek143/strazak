/**
 * @file mlx90614_baa.h
 *
 * @date 22.10.2015
 *
 * @author Jacek Jankowski
 */

#ifndef APPLICATION_USER_MLX90614_BAA_H_
#define APPLICATION_USER_MLX90614_BAA_H_

#include "stm32f1xx_hal.h"
#include <stdint.h>
#include "crc.h"

float mlx90614_baa_return_temp(uint16_t temp_data);
int mlx90614_baa_read(I2C_HandleTypeDef * phi2c, uint16_t * pta, uint16_t * pto);
int mlx90614_baa_setAddress(I2C_HandleTypeDef * phi2c, uint8_t newAdd);
void mlx90614_baa_change_used_device(uint16_t new_device_address);

#endif /* APPLICATION_USER_MLX90614_BAA_H_ */
