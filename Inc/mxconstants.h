/**
  ******************************************************************************
  * File Name          : mxconstants.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define LIN1_Pin GPIO_PIN_13
#define LIN1_GPIO_Port GPIOC
#define LIN2_Pin GPIO_PIN_14
#define LIN2_GPIO_Port GPIOC
#define LED_Pin GPIO_PIN_2
#define LED_GPIO_Port GPIOA
#define BUTTON_Pin GPIO_PIN_3
#define BUTTON_GPIO_Port GPIOA
#define T3_Pin GPIO_PIN_5
#define T3_GPIO_Port GPIOA
#define T0_Pin GPIO_PIN_6
#define T0_GPIO_Port GPIOA
#define T1_Pin GPIO_PIN_7
#define T1_GPIO_Port GPIOA
#define T2_Pin GPIO_PIN_0
#define T2_GPIO_Port GPIOB
#define SCL_Pin GPIO_PIN_10
#define SCL_GPIO_Port GPIOB
#define SDA_Pin GPIO_PIN_11
#define SDA_GPIO_Port GPIOB
#define EXTINGUISHER_Pin GPIO_PIN_12
#define EXTINGUISHER_GPIO_Port GPIOB
#define TRIG_Pin GPIO_PIN_15
#define TRIG_GPIO_Port GPIOB
#define ECHO_Pin GPIO_PIN_8
#define ECHO_GPIO_Port GPIOA
#define TX_Pin GPIO_PIN_9
#define TX_GPIO_Port GPIOA
#define RX_Pin GPIO_PIN_10
#define RX_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define LENA_Pin GPIO_PIN_15
#define LENA_GPIO_Port GPIOA
#define LENB_Pin GPIO_PIN_3
#define LENB_GPIO_Port GPIOB
#define RENB_Pin GPIO_PIN_4
#define RENB_GPIO_Port GPIOB
#define RENA_Pin GPIO_PIN_5
#define RENA_GPIO_Port GPIOB
#define PWML_Pin GPIO_PIN_6
#define PWML_GPIO_Port GPIOB
#define PWMR_Pin GPIO_PIN_7
#define PWMR_GPIO_Port GPIOB
#define RIN2_Pin GPIO_PIN_8
#define RIN2_GPIO_Port GPIOB
#define RIN1_Pin GPIO_PIN_9
#define RIN1_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
