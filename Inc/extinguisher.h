/**
 * @file extinguisher.h
 *
 * @date 19.11.2015
 *
 * @author Jacek Jankowski
 */

#ifndef EXTINGUISHER_HH
#define EXTINGUISHER_HH

#include "stm32f1xx_hal.h"
#include <stdbool.h>

extern int exti_flag;
extern uint16_t tab_ir[4];
extern uint16_t tab_temp_obj[4];
extern uint16_t tab_temp_amb[4];

typedef enum {extinguish, go, rotate_left, rotate_right, go_slow, turn_left, turn_right, turn_left_slow, turn_right_slow} decision_t;

void extinguisher(bool turn);
void led(bool turn);
decision_t decide(void);
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
void read_sensors(void);

#endif /* APPLICATION_USER_MLX90614_BAA_H_ */
